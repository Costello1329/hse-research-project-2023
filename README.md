# Кластеръ-анализъ на основѣ дополнительнаго критерія: изслѣдоватескій проектъ

Репозиторій содержитъ результаты изсѣдованія алгоритмовъ кластеризаціи, основанныхъ на аномальномъ кластерингѣ.

## Матеріалы
Матеріалы взяты изъ открытыхъ источниковъ:
- [Intelligent Choice of the Number of Clusters in K-Means Clustering: An Experimental Study with Different Cluster Spreads](https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=885a2077c92ebae06703936444945c11df27deba)
- [Bisecting K-Means and 1D Projection Divisive Clustering: A Unified Framework and Experimental Comparison](https://publications.hse.ru/pubs/share/folder/3os2f56gdm/139227037.pdf)
- [Exploring patterns of corporate social responsibility using a complementary K-means clustering criterion](https://publications.hse.ru/pubs/share/direct/403821840.pdf)

## Копирайтъ

![Creative Commons Licence](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

Всѣ матеріалы доступны по лицензіи `Creative Commons «Attribution-ShareAlike» 4.0.`
При заимствованіи любыхъ матеріаловъ изъ даннаго репозиторія, необходимо оставить ссылку на него, а также указать моё имя: _Константинъ Григорьвичъ Леладзе_.

© _Константинъ Григорьевичъ Леладзе_, **2023**

